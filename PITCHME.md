
## Systemadministration
#### Vorlesung

---
## Einführung

---
## Servus und Griaß eich

* Name: Roland Stumpner
* Systemadministration a bisserl weniger als 20 Jahre
* 10 Jahre Betreuung von ISPs
* Seit zirka 10 Jahren an der FH-OOE im Bereich Netzwerk und

![ICH](_images/cap-theorem.png)

---
## Beispiel YAML

```YAML
name: "Roland Stumpner"
todos:
  - code
  - eat
  - sleep
```

---
## Cheatsheet Example
* Grundkenntnisse in der Funktionsweise von Computern
* Grundkenntnisse in Linux und Windows Betriebsystemen
* Grundkenntnisse im Bereich Netzwerk

---
## SAM Ziele
* Grundkenntnisse im Bereich Rechnzentrumsbetrieb
* Basiswissen bei Virtualiserung und Containern
* Grundkenntnisse beim Thema Storage Speicher und Datensystemen
* Moderne Methoden zum Betrieb dieser Basis Infrastruktur (Devops)

---
## Was machen wir in SAM .... nicht!
* Step by Step Anleitungen für Installation oder Konfiguration von Systemen oder Diensten
* Wie Betriebsysteme funktionieren oder diese Bedient werden
* Wie Programmiersprachen oder Scripts geschrieben werden
* Kryptographische Grundlagen

---
## Was machen wir in SAM ?
* Planung und Dokumentation von (Server) Systemen und Services
* Konzepte und Methoden zum Betrieb von Sytemen
* Konfigurationsmanagement
* Virtualisierung und Container
* Storage und Dateisysteme

---
## Houskeeping
* Moodle als Onlineplattform
* Fragen bitte gleich
* Belohnung fürs Mitdenken / Intelligente Frage ?
* Nicht vergessen Evaluierung in LEVIS

---
## Organisatorisches Benotung (Vorlesung)
* Schriftliche Prüfung am Ende des Semesters ( 10 Fragen)
* Prüfungsstoff: Inhalt der Vorlesung als Grundlage dienen die Präsentationen und mein Vortrag

---
## Anmerkungen
* (Leider) kein Skriptum
* Vorlesungsorientierte Fertigstellung der Folien
* Kritik und Verbesserungsvorschläge bitte offen aussprechen
* LVA Bewertung über LEVIS am Ende des Semesters nicht Vergessen
